using UnityEngine;

public class leonardogrady : MonoBehaviour
{
    public AudioClip clickSound, cameraSound;

    public static leonardogrady USE;

    private AudioSource mildredmiller;

    private void Awake()
    {
       
        if (USE == null)
        {
            USE = this;
            DontDestroyOnLoad(gameObject);

            mildredmiller = transform.GetChild(0).GetComponent<AudioSource>();

            monicamack();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void monicamack()
    {
        
        AudioListener.volume = PlayerPrefs.GetInt("MusicSetting", 1);
    }

    public void winifredmarrero()
    {
        AudioListener.volume = AudioListener.volume == 1 ? 0 : 1;

        PlayerPrefs.SetInt("MusicSetting", (int)AudioListener.volume);
        PlayerPrefs.Save();
    }

    public void janetteshelton(AudioClip clip)
    {
        mildredmiller.PlayOneShot(clip);
    }
}
