using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class roscoeholloway : MonoBehaviour
{
    public Camera cameraObj;
    public gregstevens coloringMenu, paintingMenu;

    [System.Serializable]
    public class gregstevens
    {
        public GameObject aprilburnett;
        public Color color;
        public Image image;
        public Sprite murielarnold;
        public Sprite lorenefrancisco;
    }

    void Awake()
    {
        Camera.main.aspect = 16 / 9f;
    }

    void Start()
    {
        OnMenuButtonClicked(false);
    }

    public void OnMenuButtonClicked(bool isPainting)
    {
        PlayerPrefs.SetInt("isPainting", isPainting ? 1 : 0);
        PlayerPrefs.Save();

        paintingMenu.aprilburnett.SetActive(isPainting);
        coloringMenu.aprilburnett.SetActive(!isPainting);

        cameraObj.backgroundColor = isPainting ? paintingMenu.color : coloringMenu.color;
        paintingMenu.image.sprite = isPainting ? paintingMenu.murielarnold : paintingMenu.lorenefrancisco;
        coloringMenu.image.sprite = !isPainting ? coloringMenu.murielarnold : coloringMenu.lorenefrancisco;
    }

    public void kathleenholmes()
    {

    }

    public void lorrainebradford()
    {
        if (gilbertokirby.Instance.marquitang)
        {
            SceneManager.LoadScene("gms");

        }
    }
}
