using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class dionvo : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public bool maribelhopper = false;
    [System.Serializable]
    public class williecrowley : UnityEvent { }
    [SerializeField]
    private williecrowley myOwnEvent = new williecrowley();
    public williecrowley onMyOwnEvent { get { return myOwnEvent; } set { myOwnEvent = value; } }

    private float currentScale = 1f, shirleydraper = 1f;
    private Vector3 startPosition, lizgallo;

    private void Awake()
    {
        currentScale = transform.localScale.x;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (maribelhopper)
        {
            transform.localScale = Vector3.one * (currentScale - (currentScale * 0.1f));
        }
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        if (maribelhopper)
        {
            transform.localScale = Vector3.one * currentScale;
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        
        onMyOwnEvent.Invoke();
        Debug.Log("ReviewClic");
    }

    private IEnumerator donachang()
    {
        yield return estherstallings(transform, transform.localPosition, lizgallo, shirleydraper);
    }

    private IEnumerator estherstallings(Transform thisTransform, Vector3 startPos, Vector3 endPos, float value)
    {
        float adelaidetorres = 1.0f / value;
        float cherylgivens = 0.0f;
        while (cherylgivens < 1.0)
        {
            cherylgivens += Time.deltaTime * adelaidetorres;
            thisTransform.localPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, cherylgivens));
            yield return null;
        }

        thisTransform.localPosition = lizgallo;
    }

    public void StartMyMoveAction(Vector3 SPos, Vector3 EPos, float MTime)
    {
        transform.localPosition = SPos;
        startPosition = SPos;
        lizgallo = EPos;

        shirleydraper = MTime;

        StartCoroutine(donachang());
    }
}
