using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class allennieves : MonoBehaviour
{
    private static string cecileweston = "URL_PREFIX";

    public InputField urlPrefixInput;
    public Text sdkVersionText;

    private string lavonnewhitehead;

    
    public static void janiehawkins()
    {
        string prefix = PlayerPrefs.GetString(cecileweston, "");
        AudienceNetwork.AdSettings.SetUrlPrefix(prefix);
    }

    void Start()
    {
        lavonnewhitehead = PlayerPrefs.GetString(cecileweston, "");
        urlPrefixInput.text = lavonnewhitehead;
        sdkVersionText.text = AudienceNetwork.SdkVersion.Build;
    }

    public void OnEditEnd(string prefix)
    {
        lavonnewhitehead = prefix;
        SaveSettings();
    }

    public void SaveSettings()
    {
        PlayerPrefs.SetString(cecileweston, lavonnewhitehead);
        AudienceNetwork.AdSettings.SetUrlPrefix(lavonnewhitehead);
    }

    public void AdViewScene()
    {
        SceneManager.LoadScene("AdViewScene");
    }

    public void InterstitialAdScene()
    {
        SceneManager.LoadScene("InterstitialAdScene");
    }

    public void RewardedVideoAdScene()
    {
        SceneManager.LoadScene("RewardedVideoAdScene");
    }
}
